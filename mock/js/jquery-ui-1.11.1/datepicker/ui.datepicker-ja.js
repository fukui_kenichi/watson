jQuery(function($){
	$.datepicker.regional['ja'] = {
		closeText: '閉じる',
		prevText: '&#x3c;前',
		nextText: '次&#x3e;',
		currentText: '今日',
		monthNames: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
		monthNamesShort: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		dateFormat: 'yy/mm/dd', firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		duration: "",
		yearSuffix: '年',
		changeMonth: true,
		changeYear: true,
		yearRange: '2010:2030',
		// 土日祝対応
		beforeShowDay: function(date){
			var weeks = date.getDay();
			var hol_name = "";
			if(date.isJpHoliday()) {
				hol_name = date.jp_hol_name;
				weeks = 7;
			}
			if (weeks == 7) {
				return [true, 'holiday', hol_name];
			} else if (weeks == 0) {
				return [true,'sunday'];
			} else if (weeks == 6) {
				return [true, 'saturday'];
			}
			return [true, 'weekday'];
		}
	};
	$.datepicker.setDefaults($.datepicker.regional['ja']);
});