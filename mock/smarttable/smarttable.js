// JavaScript Document
$(function() {
	setScrollBox();	// 初期設定

	// スクロール同期設定バインド
	$(".smart-table-body").scroll(function(ev) {
		$(".smart-table-header").scrollLeft($(".smart-table-body").scrollLeft());
	});

	// ソート初期準備
	sortColCount = 1;
	$(".smart-table-header table th.sort").each(function(){
		$(this).attr("col_order", "");
		$(this).attr("id", "sort_col_" + sortColCount);
		targetDiv = "th#"+this.id+" div";
		text = $(targetDiv).text();
		$(targetDiv).html(text+"<span></span>");
		sortColCount++;
	});

	// ソート機能バインド
	$(".smart-table-header table th.sort").click(function(ev) {
		// 行をゲット
		colOrder = $(this).attr("col_order");
	
		headerClear();

		// 変更するspan
		targetSpan = "th#"+this.id+" div span";

		// 昇順降順処理
		if (colOrder == "asc") { // 今昇順なら降順に
			$(this).attr("col_order","desc");
			$(targetSpan).text(" ↓");
		} else { // 今昇順以外なら昇順に
			$(this).attr("col_order","asc");
			$(targetSpan).text(" ↑");
		}
	});
});

// ウインドリサイズ時
$(window).resize(function(){
	setScrollBox();
});

// ウィンドウ幅高再計算
function setScrollBox() {
	var w = window.innerWidth ? window.innerWidth: $(window).width();
	var h = window.innerHeight ? window.innerHeight: $(window).height();

	// 高さ：指定なし
//	$(".smart-table-body").height(h-250);

	// 幅：bodyの縦スクロールが表示されているかどうかを考慮
	$(".smart-table").width(w-40);　// 全体
	if ($(".smart-table-body").height() < $(".smart-table-body table").height()) {
		$(".smart-table-header").width(w-56);　// ヘッダー
		$(".smart-table-body").width(w-40);		// ボディ
		$(".smart-table-footer").width(w-56);	// フッター
	} else {
		$(".smart-table-header").width(w-40);　// ヘッダー
		$(".smart-table-body").width(w-40);		// ボディ
		$(".smart-table-footer").width(w-40);	// フッター
	}
}

// ヘッダー行ソート項目のソート情報初期化
function headerClear() {
	$(".smart-table-header table th.sort").each(function(){
		$(this).attr("col_order", "");
		targetSpan = "th#"+this.id+" div span";
		$(targetSpan).text("");
	});

	return true;
}
